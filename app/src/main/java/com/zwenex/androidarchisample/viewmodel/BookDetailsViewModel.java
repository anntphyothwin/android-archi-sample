package com.zwenex.androidarchisample.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.zwenex.androidarchisample.BookApplication;
import com.zwenex.androidarchisample.db.AuthorRepository;
import com.zwenex.androidarchisample.db.BookRepository;
import com.zwenex.androidarchisample.db.entity.AuthorEntity;
import com.zwenex.androidarchisample.db.entity.BookEntity;

/** ViewModel for BookDetailsActivity */
public class BookDetailsViewModel extends AndroidViewModel{
    private BookApplication application;
    private BookRepository bookRepo;
    private AuthorRepository authorRepo;

    public LiveData<BookEntity> book;
    public LiveData<AuthorEntity> author;

    /**
     * Constructor
     * Get BookRepository instance and sets to variable bookRepo.
     * Get AuthorRepository instance and sets to variable authorRepo.
     */
    public BookDetailsViewModel(@NonNull Application application) {
        super(application);
        this.application = (BookApplication) application;
        this.bookRepo = BookRepository.getInstance(this.application);
        this.authorRepo = AuthorRepository.getInstance(this.application);
    }

    /** Sets the book LiveData. */
    public void getBook(int bookId){
        this.book = bookRepo.getBook(bookId);
    }

    /** Sets the author LiveData. */
    public void getAuthor(int authorId){
        this.author = authorRepo.getAuthor(authorId);
    }

    /** Fetch data from network and update database */
    public void refreshBookList(){
        bookRepo.refreshBookList();
    }

    public void refreshAuthorList(){
        authorRepo.refreshAuthorList();
    }
}
