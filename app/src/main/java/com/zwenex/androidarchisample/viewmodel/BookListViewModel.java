package com.zwenex.androidarchisample.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.widget.Toast;

import com.zwenex.androidarchisample.BookApplication;
import com.zwenex.androidarchisample.db.AppDatabase;
import com.zwenex.androidarchisample.db.AuthorRepository;
import com.zwenex.androidarchisample.db.BookRepository;
import com.zwenex.androidarchisample.db.entity.AuthorEntity;
import com.zwenex.androidarchisample.db.entity.BookEntity;
import com.zwenex.androidarchisample.network.AuthorApiService;
import com.zwenex.androidarchisample.network.BookApiService;
import com.zwenex.androidarchisample.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/** ViewModel for BookListActivity */
public class BookListViewModel extends AndroidViewModel {

    private BookApplication application;
    private BookRepository bookRepo;
    private AuthorRepository authorRepo;

    public LiveData<List<BookEntity>> bookList;


    /**
     * Constructor
     * Get BookRepository instance and sets to variable bookRepo.
     * Get AuthorRepository instance and sets to variable authorRepo.
     */
    public BookListViewModel(Application application){
        super(application);
        this.application = (BookApplication)application;
        bookRepo = BookRepository.getInstance(this.application);
        authorRepo = AuthorRepository.getInstance(this.application);
    }

    public LiveData<AuthorEntity> getEachAuthor(int authorId){
        return authorRepo.getAuthor(authorId);
    }

    /** Set bookList LiveData */
    public void getBooks(){
        bookList = bookRepo.getAllBooks();
    }

    /** Fetch data from network and update database */
    public void refreshBookList(){
         bookRepo.refreshBookList();
    }
    public void refreshAuthorList(){
        authorRepo.refreshAuthorList();
    }
}
