package com.zwenex.androidarchisample.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.zwenex.androidarchisample.BookApplication;
import com.zwenex.androidarchisample.db.AuthorRepository;
import com.zwenex.androidarchisample.db.BookRepository;
import com.zwenex.androidarchisample.db.entity.AuthorEntity;

import java.util.List;

/** ViewModel for NewBookActivity */
public class NewBookViewModel extends AndroidViewModel {

    private BookApplication application;
    private BookRepository bookRepo;
    private AuthorRepository authorRepo;

    public LiveData<List<AuthorEntity>> authorList;

    /**
     * Constructor
     * Get BookRepository instance and sets to variable bookRepo.
     * Get AuthorRepository instance and sets to variable authorRepo.
     */
    public NewBookViewModel(@NonNull Application application) {
        super(application);
        this.application = (BookApplication) application;
        this.bookRepo = BookRepository.getInstance(this.application);
        this.authorRepo = AuthorRepository.getInstance(this.application);
    }

    /** Set authorList LiveData */
    public void getAuthors(){
        authorList = authorRepo.getAllAuthors();
    }

    /** Fetch data from network and update database. */
    public void refreshBookList() {
        bookRepo.refreshBookList();
    }
    public void refreshAuthorList(){
        authorRepo.refreshAuthorList();
    }


}
