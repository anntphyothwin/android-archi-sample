package com.zwenex.androidarchisample;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Required to use Glide library.
 */
@GlideModule
public class BookApplicationGlideModule extends AppGlideModule {
}
