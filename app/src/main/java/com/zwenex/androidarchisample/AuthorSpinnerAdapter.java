package com.zwenex.androidarchisample;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zwenex.androidarchisample.db.entity.AuthorEntity;

import java.util.List;

/**
 * Custom Spinner Adapter for Author Spinner
 */
public class AuthorSpinnerAdapter extends ArrayAdapter<AuthorEntity> {
    private List<AuthorEntity> authors;

    /** Constructor */
    public AuthorSpinnerAdapter(@NonNull Context context, int textViewResourceId, @NonNull List<AuthorEntity> authors) {
        super(context, textViewResourceId, authors);
        this.authors = authors;
    }

    /** Returns number of authors in Spinner */
    @Override
    public int getCount() {
        return authors.size();
    }

    /** Returns selected author. */
    @Nullable
    @Override
    public AuthorEntity getItem(int position) {
        return authors.get(position);
    }

    /** Returns ID of selected author. */
    @Override
    public long getItemId(int position) {
        return (long) authors.get(position).getId();
    }

    /** Returns a TextView for selected item in Spinner. */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(authors.get(position).getName());

        return label;
    }


    /** Returns a TextView for list of items in Spinner. */
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(authors.get(position).getName());

        return label;
    }
}
