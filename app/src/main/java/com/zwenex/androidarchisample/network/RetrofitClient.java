package com.zwenex.androidarchisample.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * RetrofitClient is used to make Retrofit instances.
 * Uses Singleton design pattern. (Explained in AppDatabase)
 */
public class RetrofitClient {
    private static Retrofit instance;

    /** API base URL */
    private static String BASE_URL = "http://bookapi.tiide.org";

    /** Checks if an instance already exists, and if not, creates a new instance and returns it.*/
    public static Retrofit getInstance(){
        if(instance == null){
            instance = buildRetrofitClient();
        }
        return instance;
    }

    /** Make constructor private to prevent having multiple instances. */
    private static Retrofit buildRetrofitClient(){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
