package com.zwenex.androidarchisample.network;

import com.zwenex.androidarchisample.db.entity.BookEntity;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Network API Service for Book.
 * All APIs for Book are defined here.
 */
public interface BookApiService {

    /**
     * Use @GET annotation to make a GET Request.
     * If the api requires a parameter, use @Query(param_name).
     */
    @GET("api/v1/getBook")
    Call<BookEntity> getBook(@Query("id") int id);

    @GET("api/v1/getBooks")
    Call<List<BookEntity>> getBooks();

    /**
     * Use @POST annotation to make a POST Request.
     * The @Multipart and @Part annotations are required only when making file uploads.
     * If your post request does not have file upload you use like:
     *
     *  @FormUrlEncoded
     *  @POST("api/v1/createBook")
     *  Call<BookEntity> createBook(
     *      @Field("title") String title,
     *      @Field("description") String description
     *  )
     */
    @Multipart
    @POST("api/v1/createBook")
    Call<BookEntity> createBook(
            @Part("title") RequestBody title,
            @Part("description") RequestBody description,
            @Part("author_id") RequestBody authorId,
            @Part MultipartBody.Part cover
    );
}
