package com.zwenex.androidarchisample.network;

import com.zwenex.androidarchisample.db.entity.AuthorEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Network API Service for Author.
 * All APIs for Author are defined here.
 */
public interface AuthorApiService {

    /**
     * Use @GET annotation to make a GET Request
     * If the api requires a parameter, use @Query(param_name)
     */
    @GET("api/v1/getAuthor")
    Call<AuthorEntity> getAuthor(@Query("id") int id);

    @GET("api/v1/getAuthors")
    Call<List<AuthorEntity>> getAuthors();
}
