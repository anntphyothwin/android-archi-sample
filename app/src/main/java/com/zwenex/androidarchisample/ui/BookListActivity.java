package com.zwenex.androidarchisample.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.zwenex.androidarchisample.db.entity.AuthorEntity;
import com.zwenex.androidarchisample.viewmodel.BookListViewModel;
import com.zwenex.androidarchisample.R;
import com.zwenex.androidarchisample.databinding.ActivityBookListBinding;
import com.zwenex.androidarchisample.db.entity.BookEntity;

import java.util.List;

/**
 * BookListActivity
 * Launcher Activity
 * Implements BooksRecyclerListener to handle RecycleView item click.
 */
public class BookListActivity extends AppCompatActivity implements BooksRecyclerViewAdapter.BooksRecyclerClickListener {

    private BookListViewModel viewModel;
    private BooksRecyclerViewAdapter adapter;
    private ActivityBookListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Binds the layout.
         * Use DataBinding to replace findViewById.
         */
        binding = DataBindingUtil.setContentView(this, R.layout.activity_book_list);

        /**
         * Each Activity/Fragment must have it's own ViewModel.
         * ViewModel holds all data required to show in UI.
         */
        viewModel = ViewModelProviders.of(this).get(BookListViewModel.class);

        /** Set Layout Manager for RecyclerView. */
        binding.booksRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        /** Create and set RecyclerView Adapter. */
        adapter = new BooksRecyclerViewAdapter(this);
        binding.booksRecyclerView.setAdapter(adapter);

        viewModel.refreshBookList();
        viewModel.refreshAuthorList();
        viewModel.getBooks();

        /**
         * Observes the bookList LiveData from the ViewModel.
         * LiveData must be observed and returns realtime data changes.
         */
        viewModel.bookList.observe(this, new Observer<List<BookEntity>>() {
            @Override
            public void onChanged(@Nullable List<BookEntity> bookEntities) {
                for(BookEntity book: bookEntities){
                    if (book.getAuthor()==null) {
                        viewModel.getEachAuthor(book.getAuthorId()).observe(BookListActivity.this, new Observer<AuthorEntity>() {
                                    @Override
                                    public void onChanged(@Nullable AuthorEntity authorEntity) {
                                        book.setAuthor(authorEntity);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                        );
                    }
                }
                adapter.refreshData(bookEntities);

                /** Stops the refreshing indicator when data refresh is finished */
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });

        /** Listener for swipe down refresh */
        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            viewModel.refreshBookList();
        });

        /** Click listener for Add Floating Action Button. */
        binding.addBtn.setOnClickListener(v -> {
            /** Created a new activity to add new book */
            Intent intent = new Intent(this, NewBookActivity.class);

            /** Starts the activity */
            startActivity(intent);
        });
    }

    /** Handles RecyclerView Item Click */
    @Override
    public void onBookCardClick(BookEntity book) {
        /** Creates a new activity to show book details. */
        Intent intent = new Intent(this, BookDetailsActivity.class);

        /** Passes the selected book ID as an extra to the new activity. */
        intent.putExtra("bookId", book.getId());

        /** Starts the activity */
        startActivity(intent);
    }
}
