package com.zwenex.androidarchisample.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.zwenex.androidarchisample.GlideApp;
import com.zwenex.androidarchisample.R;
import com.zwenex.androidarchisample.databinding.ActivityBookDetailsBinding;
import com.zwenex.androidarchisample.db.entity.AuthorEntity;
import com.zwenex.androidarchisample.db.entity.BookEntity;
import com.zwenex.androidarchisample.network.RetrofitClient;
import com.zwenex.androidarchisample.viewmodel.BookDetailsViewModel;

/**
 * Book Details Activity
 * Shows details of selected book from book list.
 */
public class BookDetailsActivity extends AppCompatActivity {

    private int bookId;
    private BookDetailsViewModel viewModel;
    private ActivityBookDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Binds the layout.
         * Use DataBinding to replace findViewById.
         */
        binding = DataBindingUtil.setContentView(this, R.layout.activity_book_details);

        /**
         * Each Activity/Fragment must have it's own ViewModel.
         * ViewModel holds all data required to show in UI.
         */
        viewModel = ViewModelProviders.of(this).get(BookDetailsViewModel.class);

        /** Gets the book ID extra value, passed from the parent activity */
        bookId = getIntent().getIntExtra("bookId",0);

        viewModel.getBook(bookId);

        /**
         * Observes the book LiveData from the ViewModel.
         * LiveData must be observed and returns realtime data changes.
         */
        viewModel.book.observe(this, new Observer<BookEntity>() {
            @Override
            public void onChanged(@Nullable BookEntity bookEntity) {
                if (bookEntity!=null) {
                    setTitle(bookEntity.getTitle());
                    binding.bookTitle.setText(bookEntity.getTitle());
                    binding.bookDescription.setText(bookEntity.getDescription().replace("\\n", "\n"));

                    /** Uses Glide Library to load image into imageView using URL */
                    GlideApp.with(BookDetailsActivity.this)
                            .load(RetrofitClient.getInstance().baseUrl() + bookEntity.getCoverUrl())
                            .into(binding.bookCover);


                    viewModel.getAuthor(bookEntity.getAuthorId());

                    /** Observes the author LiveData from the ViewModel. */
                    viewModel.author.observe(BookDetailsActivity.this, new Observer<AuthorEntity>() {
                        @Override
                        public void onChanged(AuthorEntity authorEntity) {
                            binding.authorName.setText(authorEntity.getName());
                            binding.authorAge.setText(String.valueOf(authorEntity.getAge()));
                        }
                    });
                }
            }
        });

    }
}
