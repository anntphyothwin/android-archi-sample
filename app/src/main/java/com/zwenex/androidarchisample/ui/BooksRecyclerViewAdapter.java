package com.zwenex.androidarchisample.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zwenex.androidarchisample.GlideApp;
import com.zwenex.androidarchisample.R;
import com.zwenex.androidarchisample.databinding.ItemBookBinding;
import com.zwenex.androidarchisample.db.entity.BookEntity;
import com.zwenex.androidarchisample.network.RetrofitClient;

import java.util.List;

/** RecyclerView Adapter for BookList */
public class BooksRecyclerViewAdapter extends RecyclerView.Adapter<BooksRecyclerViewAdapter.BookViewHolder> {
    private List<BookEntity> books;
    private BooksRecyclerClickListener clickListener;

    /** Constructor */
    public BooksRecyclerViewAdapter(BooksRecyclerClickListener listener){
        this.clickListener = listener;
    }

    /** Refresh recycler view with changed data */
    public void refreshData(List<BookEntity> books){
        this.books = books;
        notifyDataSetChanged();
    }

    /**
     * UI Views for each recycler view item is called a ViewHolder.
     * BookViewHolder class is set as the ViewHolder for this RecyclerView.
     */
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {

        /** Select the book entity from the position of the list */
        BookEntity book = books.get(position);

        holder.binding.bookTitle.setText(book.getTitle());
        holder.binding.bookDescription.setText(book.getDescription().replace("\n",""));

        /** Uses Glide Library to set image to the ImageView with URL. */
        GlideApp.with(holder.itemView.getContext())
                .load(RetrofitClient.getInstance().baseUrl()+book.getCoverUrl())
                .into(holder.binding.bookCover);

        if (book.getAuthor()!=null) {
            holder.binding.bookAuthor.setText(book.getAuthor().getName());
        } else{
            holder.binding.bookAuthor.setText("");
        }

        holder.binding.bookCard.setOnClickListener(v -> {
            clickListener.onBookCardClick(book);
        });
    }

    @Override
    public int getItemCount() {
        try {
            return books.size();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * UI Views for each recycler view item is called a ViewHolder.
     * Binds the layout.
     * Databinding is used to replace findViewById.
     */
    class BookViewHolder extends RecyclerView.ViewHolder{
        private ItemBookBinding binding;

        BookViewHolder(View itemView) {
            super(itemView);
            binding = ItemBookBinding.bind(itemView);
        }
    }

    /**
     * Interface to handle item clicks.
     * Implemented on BookListActivity, where the click event is executed.
     */
    public interface BooksRecyclerClickListener {
        void onBookCardClick(BookEntity book);
    }
}
