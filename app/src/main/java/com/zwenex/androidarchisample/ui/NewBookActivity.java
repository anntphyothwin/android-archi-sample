package com.zwenex.androidarchisample.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.zwenex.androidarchisample.AuthorSpinnerAdapter;
import com.zwenex.androidarchisample.FileUtils;
import com.zwenex.androidarchisample.GlideApp;
import com.zwenex.androidarchisample.R;
import com.zwenex.androidarchisample.databinding.ActivityNewBookBinding;
import com.zwenex.androidarchisample.db.entity.AuthorEntity;
import com.zwenex.androidarchisample.db.entity.BookEntity;
import com.zwenex.androidarchisample.network.BookApiService;
import com.zwenex.androidarchisample.network.RetrofitClient;
import com.zwenex.androidarchisample.viewmodel.NewBookViewModel;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * New Book Activity
 * Form for new book upload.
 */
public class NewBookActivity extends AppCompatActivity {

    private ActivityNewBookBinding binding;
    private NewBookViewModel viewModel;

    /** Request code for starting image picker. (Can be any unique integer) */
    private final int REQ_CODE_GALLERY = 1234;

    /** Request code for requesting storage permission. (Can be any unique integer) */
    private final int PERMISSION_CODE_EXT_STORAGE = 4567;

    private File bookCover = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Binds the layout.
         * Use DataBinding to replace findViewById.
         */
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_book);

        /**
         * Each Activity/Fragment must have it's own ViewModel.
         * ViewModel holds all data required to show in UI.
         */
        viewModel = ViewModelProviders.of(this).get(NewBookViewModel.class);

        viewModel.refreshAuthorList();
        viewModel.getAuthors();

        /**
         * Observes the authorList LiveData from the ViewModel.
         * LiveData must be observed and returns realtime data changes.
         */
        viewModel.authorList.observe(this, new Observer<List<AuthorEntity>>() {
            @Override
            public void onChanged(@Nullable List<AuthorEntity> authorEntities) {
                /**
                 * Creates a Custom Spinner Adapter.
                 * Spinner shows the name of each author, but when selected, we will use
                 */
                AuthorSpinnerAdapter spinnerAdapter = new AuthorSpinnerAdapter(NewBookActivity.this, android.R.layout.simple_spinner_dropdown_item, authorEntities);
                binding.bookAuthor.setAdapter(spinnerAdapter);
            }
        });

        /**
         * Prompts request permission dialog.
         * Handles result in onRequestPermissionResult
         */
        binding.addBookCoverBtn.setOnClickListener(v -> {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE_EXT_STORAGE);
        });

        /** Click listener for Create Button */
        binding.createBtn.setOnClickListener(v -> {
            String title = binding.bookTitle.getText().toString();
            String description = binding.bookDescription.getText().toString();
            String authorId = String.valueOf(
                    binding.bookAuthor.getAdapter()
                            .getItemId(binding.bookAuthor.getSelectedItemPosition())
            );
            createBook(title, description, authorId, bookCover);
        });
    }

    private void startGalleryIntent() {
        /** Creates an Image Picker Activity */
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        if (galleryIntent.resolveActivity(this.getPackageManager()) == null) {
            return;
        }

        /** Starts the activity and add Code for handle result in onActivityResult */
        startActivityForResult(galleryIntent, REQ_CODE_GALLERY);
    }

    private void createBook(String title, String description, String authorId, File bookCover){

        /** Creates a Progress Dialog */
        ProgressDialog dialog = new ProgressDialog(this);

        /** Sets message for Progress Dialog */
        dialog.setMessage("Uploading book...");

        /** Shows the Progress Dialog */
        dialog.show();

        /** Converts Strings into RequestBody for POST request form-data */
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody authorBody = RequestBody.create(MediaType.parse("text/plain"), authorId);

        /**
         * Checks if an image is picked.
         * If picked, converts image file into MultiPart Body for POST request.
         */
        MultipartBody.Part multiPartCover = null;
        if(bookCover!=null) {
            RequestBody cover = RequestBody.create(MediaType.parse(FileUtils.getMimeType(bookCover.getAbsolutePath())), bookCover);
            multiPartCover = MultipartBody.Part.createFormData("cover", bookCover.getName(), cover);
        }

        /** Makes POST request in BookApiService through RetrofitClient instance */
        RetrofitClient.getInstance().create(BookApiService.class).createBook(titleBody,descriptionBody, authorBody, multiPartCover)
                .enqueue(new Callback<BookEntity>() {

                    /** Performs tasks inside the onResponse block if the network call is successful. */
                    @Override
                    public void onResponse(Call<BookEntity> call, Response<BookEntity> response) {
                        if(response.body()!=null){

                            /** Hides the Progress Dialog when upload completes. */
                            dialog.dismiss();
                            viewModel.refreshBookList();

                            /** Creates a new activity to show the book details. */
                            Intent intent = new Intent(NewBookActivity.this, BookDetailsActivity.class);

                            /** Passes the created book ID as an extra to the new activity. */
                            intent.putExtra("bookId", response.body().getId());

                            /** Starts the activity. */
                            startActivity(intent);

                            /** Kill the New Book Activity */
                            finish();
                        }
                    }

                    /** Performs tasks inside the onFailure block if the network call failed. */
                    @Override
                    public void onFailure(Call<BookEntity> call, Throwable t) {
                        dialog.dismiss();
                        t.printStackTrace();
                        Toast.makeText(NewBookActivity.this, "Failed to create book.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /** Handles the result from Image Picker Activity */
        if(requestCode == REQ_CODE_GALLERY && resultCode == Activity.RESULT_OK){
            /** Set selected file into local variable */
            bookCover = FileUtils.mapUriToFile(this, data.getData());

            /** Set selected image to imageView with Glide Library */
            GlideApp.with(this)
                    .load(data.getData())
                    .into(binding.bookCover);
        }
    }

    /** Handles results for permission requests */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        /**
         * Handles storage permission request.
         * Start Image Picker if permission is allowed.
         * Shows toast message if permission is denied.
         */
        if(requestCode == PERMISSION_CODE_EXT_STORAGE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            startGalleryIntent();
        } else {
            Toast.makeText(this, "Storage permission is required to upload images.", Toast.LENGTH_SHORT).show();
        }
    }
}
