package com.zwenex.androidarchisample.db;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.zwenex.androidarchisample.BookApplication;
import com.zwenex.androidarchisample.db.entity.AuthorEntity;
import com.zwenex.androidarchisample.network.AuthorApiService;
import com.zwenex.androidarchisample.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The Author Repository is responsible for all database and network tasks of the Author Entity.
 * Repository also uses Singleton design pattern (Explained in AppDatabase).
 */
public class AuthorRepository {
    private static AppDatabase db;
    private static AuthorRepository instance;

    /** Make constructor private to prevent having multiple instances. */
    private AuthorRepository(BookApplication application) {
        db = application.getDatabase();
    }

    /** Checks if an instance already exists, and if not, creates a new instance and returns it.*/
    public static AuthorRepository getInstance(BookApplication application){
        if(instance == null){
            instance = new AuthorRepository(application);
        }
        return instance;
    }

    /** Uses AsyncTask to insert authors because it can only be executed in a background thread. */
    private void insertAuthors(List<AuthorEntity> authors){
        new InsertAuthorsAsyncTask().execute(authors);
    }

    /** Get all authors from the authors table. */
    public LiveData<List<AuthorEntity>> getAllAuthors(){
        return db.authorDao().getAllAuthors();
    }

    /** Get a single author by ID from the authors table. */
    public LiveData<AuthorEntity> getAuthor(int id){
        return db.authorDao().getAuthor(id);
    }

    /** Fetch book list from network and insert into books table. */
    public void refreshAuthorList(){
        /** Uses Retrofit Library to make network calls. */
        RetrofitClient.getInstance().create(AuthorApiService.class).getAuthors().enqueue(new Callback<List<AuthorEntity>>() {

            /** Performs tasks inside the onResponse block if the network call is successful. */
            @Override
            public void onResponse(Call<List<AuthorEntity>> call, Response<List<AuthorEntity>> response) {
                if(response.body()!=null) {
                    insertAuthors(response.body());
                }
            }

            /** Performs tasks inside the onFailure block if the network call failed. */
            @Override
            public void onFailure(Call<List<AuthorEntity>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    /**
     * Insert and delete tasks can only be done in background thread. User AsyncTask to execute in background.
     * AsyncTask accepts 3 values : < Parameters, Progress, Result >
     * To keep things simple, we will only use Parameters, which is the List of AuthorEntity we want to insert.
    */
    private static class InsertAuthorsAsyncTask extends AsyncTask<List<AuthorEntity>, Void, Void> {

        @Override
        protected Void doInBackground(List<AuthorEntity>... bookEntities) {
            db.authorDao().deleteAllAuthors();
            db.authorDao().addAuthors(bookEntities[0]);
            return null;
        }
    }
}

