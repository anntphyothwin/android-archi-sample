package com.zwenex.androidarchisample.db.entity;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * BookEntity
 * Each table for the room database is defined as an entity with the Entity annotation.
 */
@Entity(tableName = "books")
public class BookEntity{

    /**
     * Primary key must have the @PrimaryKey annotation, Use @PrimaryKey(autoGenerate = true) for auto incrementing id.
     */
    @PrimaryKey
    private int id;
    private String title;
    private String description;

    /**
     * @SerializedName is used to serialize GSON(Google JSON) objects into entity
     * Example : The following JSON object has "author_id" as key, and @SerializedName("author_id") is used to serialize that key into authorId.
            {
                "id": 1,
                "title": "Harry Potter and the Sorcerer's Stone",
                "description": "Harry Potter's life is miserable.",
                "author_id": 2,
                "created_at": "2018-06-01 09:40:38",
                "cover": "/system/books/covers/000/000/006/original/Hp_1.jpg?1527846037"
            }
     */

    @SerializedName("author_id")
    private int authorId;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("cover")
    private String coverUrl;

    /**
     * Use @Ignore annotation to NOT INCLUDE this field in the table.
     */
    @Ignore
    private AuthorEntity author;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public AuthorEntity getAuthor() {
        return author;
    }

    public void setAuthor(AuthorEntity author) {
        this.author = author;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", authorId=" + authorId +
                ", createdAt='" + createdAt + '\'' +
                ", coverUrl='" + coverUrl + '\'' +
                ", author=" + author +
                '}';
    }
}
