package com.zwenex.androidarchisample.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.zwenex.androidarchisample.db.dao.AuthorDao;
import com.zwenex.androidarchisample.db.dao.BookDao;
import com.zwenex.androidarchisample.db.entity.AuthorEntity;
import com.zwenex.androidarchisample.db.entity.BookEntity;

/**
 * Main Database
 * The Database class must use Singleton design pattern and must only use a single instance in all classes.
 */
@Database(entities = {AuthorEntity.class, BookEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{
    private static AppDatabase instance;

    /** Database name*/
    public static final String DATABASE_NAME = "books-example-db";

    /** All DAOs of the tables must be added here.*/
    public abstract BookDao bookDao();
    public abstract AuthorDao authorDao();

    /** Checks if an instance already exists, and if not, creates a new instance and returns it.*/
    public static AppDatabase getInstance(final Context context) {
        if(instance == null){
            instance = buildDatabase(context.getApplicationContext());
        }
        return instance;
    }

    /** Make constructor private to prevent having multiple instances.*/
    private static AppDatabase buildDatabase(final Context appContext) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME).build();
    }
}
