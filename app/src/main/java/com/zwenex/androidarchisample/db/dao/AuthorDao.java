package com.zwenex.androidarchisample.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.zwenex.androidarchisample.db.entity.AuthorEntity;

import java.util.List;

/**
 * DAO for the Authors Table
 * Each table must have it's own DAO to insert, update, select or delete records.
 * Each Dao is implemented in the main database class (AppDatabase).
 */
@Dao
public interface AuthorDao {
    @Query("SELECT * from authors")
    LiveData<List<AuthorEntity>> getAllAuthors();

    @Query("SELECT * from authors where id = :authorId")
    LiveData<AuthorEntity> getAuthor(int authorId);

    /**
     * Using OnConflictStrategy.Replace replaces the record if a record with the same PRIMARY KEY exists
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addAuthor(AuthorEntity authorEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] addAuthors(List<AuthorEntity> authorEntities);

    @Query("DELETE from authors")
    void deleteAllAuthors();
}
