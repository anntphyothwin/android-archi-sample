package com.zwenex.androidarchisample.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.zwenex.androidarchisample.db.entity.BookEntity;

import java.util.List;

/**
 * DAO for the Books Table
 * Each table must have it's own DAO to insert, update, select or delete records.
 * Each Dao is implemented in the main database class (AppDatabase).
 */
@Dao
public interface BookDao {
    @Query("SELECT * from books")
    LiveData<List<BookEntity>> getAllBooks();

    @Query("SELECT * from books where id = :bookId")
    LiveData<BookEntity> getBook(int bookId);

    /**
     * Using OnConflictStrategy.Replace replaces the record if a record with the same PRIMARY KEY exists
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addBook(BookEntity bookEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] addBooks(List<BookEntity> bookEntities);

    @Query("DELETE from books")
    void deleteAllBooks();
}
