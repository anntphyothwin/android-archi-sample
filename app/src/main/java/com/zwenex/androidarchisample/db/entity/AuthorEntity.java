package com.zwenex.androidarchisample.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Author Entity
 * Each table for the room database is defined as an entity with the Entity annotation
 */
@Entity(tableName = "authors")
public class AuthorEntity {

    /**
     * Primary key must have the @PrimaryKey annotation
     * Use @PrimaryKey(autoGenerate = true) for auto incrementing id.
     */
    @PrimaryKey
    private int id;
    private String name;
    private int age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "AuthorEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
