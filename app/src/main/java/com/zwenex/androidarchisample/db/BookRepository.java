package com.zwenex.androidarchisample.db;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.zwenex.androidarchisample.BookApplication;
import com.zwenex.androidarchisample.db.entity.BookEntity;
import com.zwenex.androidarchisample.network.BookApiService;
import com.zwenex.androidarchisample.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The Book Repository is responsible for all database and network tasks of the Book Entity.
 * Repository also uses Singleton design pattern (Explained in AppDatabase).
 */
public class BookRepository {

    private static AppDatabase db;
    private static BookRepository instance;

    private static MutableLiveData<long[]> refreshbookResponse = new MutableLiveData<>();

    /** Make constructor private to prevent having multiple instances. */
    private BookRepository(BookApplication application){
        db = application.getDatabase();
    }

    /** Checks if an instance already exists, and if not, creates a new instance and returns it.*/
    public static BookRepository getInstance(BookApplication application) {
        if(instance == null){
            instance = new BookRepository(application);
        }
        return instance;
    }

    /** Uses AsyncTask to insert books because it can only be executed in a background thread. */
    private void insertBooks(List<BookEntity> books){
        new InsertBooksAsyncTask().execute(books);
    }

    /** Get all books from the books table. */
    public LiveData<List<BookEntity>> getAllBooks(){
        return db.bookDao().getAllBooks();
    }

    /** Get a single book by ID from the book table. */
    public LiveData<BookEntity> getBook(int id){
        return db.bookDao().getBook(id);
    }

    /** Fetch book list from network and insert into books table. */
    public LiveData<long[]> refreshBookList(){
        /** Uses Retrofit Library to make network calls. */
        RetrofitClient.getInstance().create(BookApiService.class).getBooks().enqueue(new Callback<List<BookEntity>>() {

            /** Performs tasks inside the onResponse block if the network call is successful. */
            @Override
            public void onResponse(Call<List<BookEntity>> call, Response<List<BookEntity>> response) {
                Log.e("Response", response.body().toString());
                if(response.body()!=null) {
                    insertBooks(response.body());
                }
            }

            /** Performs tasks inside the onFailure block if the network call failed. */
            @Override
            public void onFailure(Call<List<BookEntity>> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return refreshbookResponse;
    }

    /**
     * Insert and delete tasks can only be done in background thread. User AsyncTask to execute in background.
     * AsyncTask accepts 3 values : < Parameters, Progress, Result >
     * To keep things simple, we will only use Parameters, which is the List of BookEntity we want to insert.
    */
    private static class InsertBooksAsyncTask extends AsyncTask<List<BookEntity>, Void, Void> {

        @Override
        protected Void doInBackground(List<BookEntity>... bookEntities) {
            db.bookDao().deleteAllBooks();
            refreshbookResponse.postValue(db.bookDao().addBooks(bookEntities[0]));
            return null;
        }
    }

}
