package com.zwenex.androidarchisample;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.zwenex.androidarchisample.db.AppDatabase;

/**
 * Main Application
 */
public class BookApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this);
    }
}
